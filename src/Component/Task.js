import React from 'react'

class Task extends React.Component {

    constructor(props) {
        super(props)
        console.log(props, "constructor")   
        this.onFinishedTask= this.onFinishedTask.bind(this)
        this.onUnstartTask= this.onUnstartTask.bind(this)
        this.onStartTask= this.onStartTask.bind(this)
    } 
    onStartTask(e){ 
        console.log(this.props) 
        this.props.OnStart(this.props.name)
    }
    onUnstartTask(e){
        console.log(this.props) 
        this.props.OnUnstart(this.props.name)
    }
    onFinishedTask(e){
        console.log(this.props)     
        this.props.OnFinished(this.props.name)
    }
    dragging(e){
        // alert("jordan")
    }

    dragStart(e){
        // alert("Jordan drop")
        e.dataTransfer.setData("Text", e.target.id);
    }
    render(){   
        let buttons= [];
        if((this.props.statut === 1)){
            buttons.push(<button className="btn ml-1 btn-success"key="start" onClick= {this.onStartTask}>Go</button>)
        }
        if((this.props.statut === 2)){
            buttons.push(<button className="btn ml-1 btn-danger"key="unStart" onClick = {this.onUnstartTask}>Stop</button>) 
            buttons.push(<button className="btn ml-1 btn-primary"key="done" onClick = {this.onFinishedTask}>Done</button>)  
        }
        if((this.props.statut === 3 )){
            
        }
        return (
            <div 
                id={this.props.id}
                onDragStart={this.dragStart}
                onDrag={this.dragging}
                draggable={true}
            className= "container-fluid p-2 border border-primary mt-2 shadow p-3 mb-5 bg-body rounded">
                <div className= "row">
                    <div className="col-8">
                    {this.props.name}
                    </div>
                    <div className="col-4">
                    {buttons}
                    </div>
                </div>      
            </div>
        )
    }
}


export default Task;