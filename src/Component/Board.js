import React from 'react';
import List  from './List'
class Board extends React.Component {

    constructor(props) {
        super(props)
        this.state= {
            userItem: '',
            list : [ 
                { 
                    "id":1,
                    'name': "boire",
                    'statut': 1
                },

                { 
                    "id":2,
                    'name': "sortir",
                    'statut': 2,
                },

                { 
                    "id":3,
                    'name': "ecrire mon rapport ",
                    'statut': 3
                }
            ],
        }
        this.handleFinishedTask= this.handleFinishedTask.bind(this)
        this.handleUnstartTask= this.handleUnstartTask.bind(this)
        this.handleStartTask= this.handleStartTask.bind(this)
    }

    setTask(id,statut){
        let tab= this.state.list
        for(let task of tab){
            if(task.name === id){
                task.statut = statut
            }
        }
        console.log(tab,"brice",id)
        return tab;
    }

    getTodoTasks(){
        let tasks= []
        for( let task of this.state.list){
            if(task.statut === 1){
                tasks.push(task)
            }
        }

        return tasks
    }

    getOngoingTasks(){
        let tasks= []
        for( let task of this.state.list){
            if(task.statut === 2){
                tasks.push(task)
         }
        }
        return tasks
    }

    getFinishedTasks(){
        let tasks= []
        for( let task of this.state.list){
            if(task.statut === 3){
                tasks.push(task)
            }
        }
        return tasks
    }

    handleStartTask(id){
        this.setState(() => ({
            list: this.setTask(id,2)
        }));
        console.log(this.state.list,"jordan")
    }
    handleUnstartTask(id){
        this.setState(() => ({
            list: this.setTask(id,1)
        }));
        console.log("Unstart Task",id,this.props.start,this.props.ongoing)
    }
    handleFinishedTask(id){
        this.setState(() => ({
            list: this.setTask(id,3)
        }));
        console.log("End Task",id,this.props.start,this.props.ongoing,this.props.end)
    }

    render() {
        return (
            <div className="container-fluid m-0 p-0  h-100 w-100">
                <form>
                    <input type="text" placeholder="new tastk" />
                    <button>Add Task</button>
                </form>
                <div className="row m-0 h-100 w-100">
                    <List 
                        OnStartTask={this.handleStartTask}
                        tasks= {this.getTodoTasks()}
                    />
                    <List 
                        OnUnStartTask={this.handleUnstartTask} 
                        OnFinished={this.handleFinishedTask}    
                        tasks= {this.getOngoingTasks()}
                        />
                    <List Onfinished={this.handleFinishedTask} tasks= {this.getFinishedTasks()}/>
                </div>
            </div>
        )
    }
}

export default Board;