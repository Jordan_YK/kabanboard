import React from "react";
import Task from './Task';

class List extends React.Component {

    constructor(props) {
        super(props)
        console.log(props)
        this.prop= props;
        this.handleFinishedTask= this.handleFinishedTask.bind(this)
        this.handleUnstartTask= this.handleUnstartTask.bind(this)
        this.handleStartTask= this.handleStartTask.bind(this)
    } 

    handleFinishedTask(id){
        //this.props.end =true
        console.log(this.prop)
        this.prop.OnFinished(id)
        console.log("Start Task",id,this.props.start,this.props.ongoing,this.props.end)
    }
    handleStartTask(id){
        this.props.OnStartTask(id)
        console.log("Start Task",id,this.props.start,this.props.ongoing)
    }
    handleUnstartTask(id){
        this.props.OnUnStartTask(id)
        console.log("Unstart Task",id,this.props.start,this.props.ongoing)
    }   

    drop(e){
        e.preventDefault();
        let data = e.dataTransfer.getData("Text");    
        e.target.appendChild(document.getElementById(data));
    }
    allowDrop(e) {
        alert("Jordan drop")
        e.preventDefault();
        alert("jordan")
    }
    

    render(){
        let tasksElement = [];
        for(let i in this.props.tasks){
            tasksElement.push(
                <Task 
                    key={i}
                    name={this.props.tasks[i].name}
                    statut={this.props.tasks[i].statut}
                    OnStart={this.handleStartTask}
                    OnUnstart={this.handleUnstartTask}
                    OnFinished={this.handleFinishedTask}
                />
            )
        }
        return (
        <div 
            className="col-4 bg-light border border-secondary h-100"
            onDrop={this.drop}
            onDragOver={this.allowdrop}
        > 
            {/* <Task 
                    name={"Jordan"} 
                    start={false} 
                    ongoing= {false}
                    end= {false}
                    OnStartTask={this.handleStartTask}
                    OnUnstartTask={this.handleUnstartTask}
                    OnFinishedTask={this.handleFinishedTask}
                /> */}
            {tasksElement}
        </div>)
    }
}

export default List;