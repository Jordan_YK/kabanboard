import './App.css';
import './css/Style.css';
import Board from './Component/Board'

function App() {
  return (
    <div className="App h-100 w-100">
        <Board />
    </div>
  );
}

export default App;
